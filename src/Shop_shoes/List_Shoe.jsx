import React, { Component } from "react";
import Item_Shoe from "./Item_Shoe";
export default class List_Shoe extends Component {
  renderListShoe = () => {
    const { dataListShoe, handelAddShoe } = this.props;
    return dataListShoe.map((item, index) => {
      return (
        <Item_Shoe handelAddShoe={handelAddShoe} dataShoe={item} key={index} />
      );
    });
  };
  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
