import React, { Component } from "react";

export default class Cart_Shoe extends Component {
  renderGioHang = () => {
    const { dataGioHang, handelPlusQuantity, removeShoeCart } = this.props;
    return dataGioHang.map((item, index) => {
      return (
        <tr key={index}>
          <td>
            <img src={item.image} width={100} alt="" />
          </td>
          <td>{item.name}</td>
          <td>
            <button
              onClick={() => handelPlusQuantity(item.id, -1)}
              className="btn btn-danger"
            >
              -
            </button>
            {item.soLuong}
            <button
              onClick={() => handelPlusQuantity(item.id, 1)}
              className="btn btn-success"
            >
              +
            </button>
          </td>
          <td>{item.price}</td>
          <td>{item.soLuong * item.price}</td>
          <td>
            <button
              onClick={() => removeShoeCart(item.id)}
              className="btn btn-primary"
            >
              Xoá
            </button>
          </td>
        </tr>
      );
    });
  };
  renderTongTien = () => {
    let sum = 0;
    this.props.dataGioHang.forEach((item) => {
      sum += item.soLuong * item.price;
    });
    return sum;
  };
  render() {
    const { removeAllCart } = this.props;
    return (
      <>
        {/* Button trigger modal */}
        <button
          type="button"
          className="btn btn-primary sticky-top"
          data-bs-toggle="modal"
          data-bs-target="#exampleModal"
        >
          <i className="fa fa-shopping-cart"></i>{" "}
        </button>
        {/* Modal */}
        <div
          className="modal fade"
          id="exampleModal"
          tabIndex={-1}
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-fullscreen">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Giỏ Hàng
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                />
              </div>
              <div className="modal-body">
                <table className="table">
                  <thead>
                    <tr>
                      <th>Hình Ảnh</th>
                      <th>Tên</th>
                      <th>Số Lượng</th>
                      <th>Đơn Giá</th>
                      <th>Thành Tiền</th>
                      <th>Tuỳ Chỉnh</th>
                    </tr>
                  </thead>
                  <tbody>{this.renderGioHang()}</tbody>
                </table>
                <div>Tổng tiền : {this.renderTongTien()}$</div>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-bs-dismiss="modal"
                  onClick={removeAllCart}
                >
                  Thanh tóan
                </button>
                <button
                  onClick={removeAllCart}
                  type="button"
                  className="btn btn-primary"
                >
                  xoá hêt
                </button>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
