import React, { Component } from "react";
import Cart_Shoe from "./Cart_Shoe";
import { dataShoe } from "./dataShoe";
import List_Shoe from "./List_Shoe";
export default class Layout_Shoe extends Component {
  saveLocalStorage = (newGioHang) => {
    let newGioHangJson = JSON.stringify(newGioHang);
    localStorage.setItem("NewGioHang", newGioHangJson);
  };
  getDataLocalStorage = () => {
    let newGioHangLocalStorage = localStorage.getItem("NewGioHang");
    let newGioHang = JSON.parse(newGioHangLocalStorage);
    return newGioHang;
  };
  state = {
    data: dataShoe,
    gioHang: localStorage.getItem("NewGioHang")
      ? this.getDataLocalStorage()
      : [],
  };
  handelAddShoe = (shoe) => {
    let index = this.state.gioHang.findIndex((item) => item.id == shoe.id);
    let newGioHang = [...this.state.gioHang];
    if (index == -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      newGioHang.push(newShoe);
    } else {
      newGioHang[index].soLuong++;
    }
    this.saveLocalStorage(newGioHang);
    this.setState({
      gioHang: newGioHang,
    });
  };
  handelPlusQuantity = (idShoe, value) => {
    let index = this.state.gioHang.findIndex((item) => item.id == idShoe);
    let newGioHang = [...this.state.gioHang];
    if (index == -1) {
      return;
    }
    newGioHang[index].soLuong += value;
    newGioHang[index].soLuong <= 0 && newGioHang.splice(index, 1);
    this.saveLocalStorage(newGioHang);
    this.setState({
      gioHang: newGioHang,
    });
  };
  removeShoeCart = (idShoe) => {
    let index = this.state.gioHang.findIndex((item) => item.id == idShoe);
    let newGioHang = [...this.state.gioHang];
    newGioHang.splice(index, 1);
    this.saveLocalStorage(newGioHang);
    this.setState({
      gioHang: newGioHang,
    });
  };
  removeAllCart = () => {
    let newGioHang = [];
    this.saveLocalStorage(newGioHang);
    this.setState({
      gioHang: newGioHang,
    });
  };
  render() {
    return (
      <div className="container">
        <Cart_Shoe
          handelPlusQuantity={this.handelPlusQuantity}
          dataGioHang={this.state.gioHang}
          removeShoeCart={this.removeShoeCart}
          removeAllCart={this.removeAllCart}
        />
        <List_Shoe
          dataListShoe={this.state.data}
          handelAddShoe={this.handelAddShoe}
        />
      </div>
    );
  }
}
