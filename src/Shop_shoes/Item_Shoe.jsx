import React, { Component } from "react";

export default class Item_Shoe extends Component {
  render() {
    const { dataShoe, handelAddShoe } = this.props;
    return (
      <div className="col-3 py-3">
        <div className="card h-100" style={{ width: "100%" }}>
          <img src={dataShoe.image} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">{dataShoe.name}</h5>
            <p className="card-text">
              <button
                onClick={() => handelAddShoe(dataShoe)}
                className="btn btn-success"
              >
                add
              </button>
            </p>
          </div>
        </div>
      </div>
    );
  }
}
