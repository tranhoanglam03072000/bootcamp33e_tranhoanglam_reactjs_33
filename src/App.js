import logo from "./logo.svg";
import "./App.css";
import Layout_Shoe from "./Shop_shoes/Layout_Shoe";

function App() {
  return (
    <div className="App">
      <Layout_Shoe />
    </div>
  );
}

export default App;
